#!/bin/bash
#set -x

# 20220420 DAF Version inicial
#              Usar: bash $0 vm_name 13

vm_name=${1:-defaultname}
mypgversion=${2:-14}

vagrant up $vm_name
#ansible all -i ./hosts.yaml -u vagrant -m shell -a 'echo "$(hostname) - $(hostname -I)"'
ansible all -u vagrant -m shell -a 'echo "$(hostname) - $(hostname -I)"' -i $vm_name,
#ansible all -u vagrant -m ping -i $vm_name,
ansible-playbook -u vagrant postgresql.yml --extra-vars "mypgversion=$mypgversion" -i $vm_name,

